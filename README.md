# YTMusic-Playlist-Creator

## Description
Python tool to create Youtube Music playlists based upon a CSV file or folder of CSV files (e.g. playlists exported from Spotify or another media player). 

It is designed to work with tools such as [exportify](https://github.com/watsonbox/exportify) to port playlists to youtube music from other platforms (e.g. Spotify)

The intention of writing this tool was to provide an open source method of moving playlists to Youtube Music that didn't require you to blindly trust an unknown utility with your google password!

## Acknowlegements
This project is a fork of [https://github.com/dizzyspiral/spotify-to-youtube](https://github.com/dizzyspiral/spotify-to-youtube)

The [blog post](https://dev.to/dizzyspi/creating-a-spotify-to-youtube-music-playlist-converter-f39) from the original project is useful for understanding the process.

The key improvements differences between the projects are:
- Improved recognition of songs by applying filtering and grabbing Youtube's recommended result
- Songs will now be correctly added to new playlists instead a single default one (bug fix)
- Fixed bug where an extra erroneous track would be added to the playlist
- Added tool to create playlists in bulk from a folder of CSVs

## Installation

1. Clone this repository and enter it.

```bash
git clone https://gitlab.com/jonathanfox5/ytmusic-playlist-creator.git
cd ytmusic-playlist-creator
```

2. Set up virtual environment and install dependencies.

```bash
python3 venv venv
source venv/bin/activate
pip install -r REQUIREMENTS.txt
```

3. Take a copy of `example_auth.json` and update the cookie with one taken from a POST request on a logged in youtube music web page. See the aforementioned [blog post](https://dev.to/dizzyspi/creating-a-spotify-to-youtube-music-playlist-converter-f39) and [ytmusicapi documentation](https://ytmusicapi.readthedocs.io/en/latest/setup.html) for help on obtaining this. 

- For the purposes of these instructions, it's assumed that you have renamed the json file to `auth.json`. 
- There is no requirement to change any field apart from the cookie field unless the api changes.
- We are essentially following the "Manual file creation" process from the [ytmusicapi documentation](https://ytmusicapi.readthedocs.io/en/latest/setup.html).

## Usage

4. Save the playlist CSV files into a folder that can be readily accessed.

- For the purposes of these instructions, it's assumed that you have three playlists ("songs 1.csv", "songs 2.csv" and "songs 3.csv") inside a folder named "playlists" which is saved within the main "ytmusic-playlist-creator" folder where this readme resides.
- An example format for the CSV files is given in `example_playlist_format.csv`. This format mirrors that of the [exportify](https://github.com/watsonbox/exportify) tool. Only the track name and the artist name need to be provided.
- [exportify](https://github.com/watsonbox/exportify) can be used to export your playlists from Spotify into CSV so that they can be processed by this tool. A web app is linked on the github page which means that there is no requirement to install anything else.

5. Ensure that you are within the main "ytmusic-playlist-creator" folder where this readme resides and that the virtual environment that you created earlier is activated.

```bash
source venv/bin/activate
```

6. There are three main options for uploading playlists which are demonstrated through examples below.

a) To upload a single csv named "songs 1.csv" and save it to a playlist called "Awesome Tunes". See step 4 for assumptions on file naming and locations.

```bash
python main.py auth.json "playlists/song 1.csv" --playlist "Awesome Tunes"
```

b) To upload a single csv named "songs 1.csv" and save your liked songs. See step 4 for assumptions on file naming and locations.

```bash
python main.py auth.json "playlists/song 1.csv" --liked
```

c) To upload all playlist CSV files within a folder and create individual youtube music playlists for each. The playlist names will default to the filenames (without the csv file extension)

```bash
python main.py auth.json "playlists"
```
