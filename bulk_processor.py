import main
import argparse
import pathlib
import os


def get_args():
    parser = argparse.ArgumentParser(
        description="Import a Spotify playlist to YouTube Music"
    )
    parser.add_argument("auth", type=str, help="ytmusicapi auth file")
    parser.add_argument(
        "directory",
        type=str,
        help="Directory containing exported Spotify playlist CSV files",
    )

    args = parser.parse_args()

    return args


def add_to_playlist(cmd_args, file_path):

    # Name the playlist the same as the file
    playlist_name = pathlib.Path(file_path).stem

    print("Connecting to YouTube Music...")
    ytmusic = main.connect_to_yt(cmd_args)

    print("Loading exported Spotify playlist...")
    songs = main.load_spotify_playlist(file_path)

    print("Searching for songs on YouTube...")
    video_ids = main.search_for_songs(ytmusic, songs)

    print("Creating playlist...")
    playlist_id = main.make_playlist(ytmusic, playlist_name, "")

    print("Adding songs...")
    main.add_songs(ytmusic, playlist_id, video_ids)


if __name__ == "__main__":
    cmd_args = get_args()

    directory_path = os.path.abspath(cmd_args.directory)
    dir_contents = os.listdir(directory_path)
    dir_contents.sort()

    for file_path in dir_contents:

        full_path = os.path.join(directory_path, file_path)
        print("Processing " + full_path)

        if os.path.isfile(full_path):
            add_to_playlist(cmd_args, full_path)
        else:
            print("Skipping " + full_path)

    print("Done!")
